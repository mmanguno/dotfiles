import           Data.Char                    (toLower)
import           Data.Foldable                (fold)
import           Data.List                    (intercalate)
import           Data.Time.Clock              (getCurrentTime)
import           Data.Time.Format             (defaultTimeLocale, formatTime)
import           System.Directory             (getHomeDirectory)
import           System.IO                    (hPutStrLn)

import           XMonad
import           XMonad.Hooks.DynamicLog      (dynamicLogWithPP, ppCurrent,
                                               ppLayout, ppOutput, ppSep,
                                               ppTitle, ppUrgent, wrap,
                                               xmobarColor, xmobarPP)
import           XMonad.Hooks.ManageDocks     (ToggleStruts (..), avoidStruts,
                                               docks, manageDocks)
import           XMonad.Layout.NoBorders      (smartBorders)
import           XMonad.Util.EZConfig         (additionalKeys)
import           XMonad.Util.Run              (runProcessWithInput, safeSpawn,
                                               spawnPipe)

import           Graphics.X11.ExtraTypes.XF86


-- External dependencies:
--   - compton
--   - xmobar (with alsa, mpd, and xft)
--   - stalonetray
--   - xterm
--   - rofi
--   - feh
--   - amixer
--   - scrot
--   - maim
--   - i3lock
--   - acpilight


-- References:
--
-- 1. https://hackage.haskell.org/package/xmonad-contrib-0.14/docs/XMonad-Hooks-DynamicLog.html
-- 2. https://ethanschoonover.com/solarized/


-- Could replace with a dynamic search for $HOME,
-- but I don't feel like handling the IO at the
-- moment.
home = "/home/mitchell"

type Color' = String
data ColorTheme =
  ColorTheme {
      primary              :: Color'
    , secondary            :: Color'
    , emphasis             :: Color'
    , background           :: Color'
    , backgroundHighlights :: Color'
    , yellow               :: Color'
    , orange               :: Color'
    , red                  :: Color'
    , magenta              :: Color'
    , violet               :: Color'
    , blue                 :: Color'
    , cyan                 :: Color'
    , green                :: Color' }
  deriving (Eq, Show)

data XMonadConfiguration =
  XMonadConfiguration {
    audioManager    :: String
  , terminal'       :: String
  , font            :: String
  , fontSize        :: Int
  , theme           :: ColorTheme
  , locker          :: String
  , lockerOpts      :: [String]
  , volumeDelta     :: Int
  , brightnessDelta :: Int }
  deriving (Eq, Show)

solarizedDark = ColorTheme {
    primary = "#839496"
  , secondary = "#586e75"
  , emphasis = "#93a1a1"
  , background = "#002b36"
  , backgroundHighlights = "#073642"
  , yellow = "#b58900"
  , orange = "#cb4b16"
  , red = "#dc322f"
  , magenta = "#d33682"
  , violet = "#6c71c4"
  , blue = "#268bd2"
  , cyan = "#2aa198"
  , green = "#859900" }

solarizedLight = solarizedDark {
    primary = "#657b83"
  , secondary = "#93a1a1"
  , emphasis = "#586e75"
  , background = "#fdf6e3"
  , backgroundHighlights = "#eee8d5" }

defaultConfig' = XMonadConfiguration {
    audioManager = "pavucontrol"
  , terminal' = "xterm"
  , font = "Fira Code"
  , fontSize = 12
  , theme = solarizedDark
  , locker = "i3lock"  -- TODO: replace with something else
  , lockerOpts = ["-i", home ++ "/.lock-bg.png"]
  , volumeDelta = 5
  , brightnessDelta = 5}

main = xmonad' defaultConfig'

xmonad' conf = do
  xmproc <- spawnPipe $ home ++ "/.cabal/bin/xmobar -B" ++ bg
  safeSpawn "compton" ["-b"]
  safeSpawn "stalonetray" ["-bg", bg]
  autoLocker (locker c) (lockerOpts c)
  xmonad $ docks def
    {
      logHook = dynamicLogWithPP xmobarPP
                {
                  ppCurrent = xmobarColor emph ""
                , ppLayout = wrap "(layout " ")" . xmobarColor emph "" . lower
                , ppOutput = hPutStrLn xmproc . wrap "(xmonad " ")"
                , ppSep = " "
                , ppTitle  = wrap "(title " ")" . xmobarColor emph ""
                , ppUrgent = xmobarColor (red t) (yellow t)
                }
    , modMask            = mod4Mask
    , terminal           = terminal' c
    , normalBorderColor  = transparent
    , focusedBorderColor = emph
    , startupHook        = spawn $ home ++ "/.fehbg"
    , manageHook         = manageDocks
                           <+> manageHook def
    , layoutHook         = avoidStruts $ smartBorders $ layoutHook def
    } `additionalKeys`
    [
    -- Toggle borders/xmobar ("struts")
      ((mod4Mask, xK_b              ), sendMessage ToggleStruts
                                       *> toggleTray t                        )
    -- Lock screen
    , ((mod4Mask .|. shiftMask, xK_l), safeSpawn (locker c) (lockerOpts c)    )
    -- Screen capturing
    , ((0, xK_Print                 ), printScreen                            )
    , ((shiftMask, xK_Print         ), captureSelection                       )
    -- Spawn dmenu
    , ((mod4Mask, xK_p              ), spawnRofi (font c) (fontSize c)        )
    -- Audio controls
    , ((0, xF86XK_AudioRaiseVolume  ), raiseVolumeBy . volumeDelta $ c        )
    , ((0, xF86XK_AudioLowerVolume  ), lowerVolumeBy . volumeDelta $ c        )
    , ((0, toggleMicKey             ), toggleMic                              )
    , ((0, xF86XK_AudioMute         ), toggleMute                             )
    -- Open audio manager
    , ((shiftMask, xF86XK_AudioMute ), safeSpawn (audioManager c) mempty      )
    -- Brightness controls
    , ((0, xF86XK_MonBrightnessUp   ), raiseBrightnessBy . brightnessDelta $ c)
    , ((0, xF86XK_MonBrightnessDown ), lowerBrightnessBy . brightnessDelta $ c)
    -- Automatically set/reset screen mirroring
    , ((0, xF86XK_Display           ), toggleScreenLayout                     )
    ]
  where
    c = conf
    t = theme conf
    emph = emphasis . theme $ conf
    fg = primary . theme $ conf
    bg = background . theme $ conf

-- | Cycles screen layouts across displays; good for connecting to monitors.
toggleScreenLayout :: MonadIO m => m ()
toggleScreenLayout = safeSpawn "xrandr" mempty

-- | Toggles the system tray on/off.
toggleTray :: MonadIO m => ColorTheme -> m ()
toggleTray c = go =<< running "stalonetray"
  where
    go :: MonadIO m => Bool -> m ()
    go True  = safeSpawn "stalonetray" ["-bg", bg]
    go False = killAll "stalonetray"
    bg = background c

-- | Kills all instances of a named process.
killAll :: MonadIO m => String -> m ()
killAll = safeSpawn "pkill" . pure

-- | Hex code for transparency.
transparent :: String
transparent = "#000000"

-- | Toggle mic key on my T480/T420
toggleMicKey :: KeySym
toggleMicKey = 0x1008FFB2

-- | Lowercases a string.
-- ex. lower "Hello World" = "hello world"
lower :: String -> String
lower = fmap toLower

-- | Gets the current system time formatted as %FT%T%QZ.
now :: MonadIO m => m String
now = liftIO $ format <$> getCurrentTime
  where format = formatTime defaultTimeLocale "%FT%T%QZ"

-- | Start the autolocker with a given locker and locker options.
autoLocker :: MonadIO m => String -> [String] -> m ()
autoLocker l lOpts= safeSpawn "xautolock" opts
  where opts = [
            "-secure"
          , "-detectsleep"
          , "-time", "5"
          , "-corners", "0-00"
          , "-locker", "\"" ++ locker' ++ "\""
          , "-killer", "\"systemctl suspend\""
          , "-killtime", "10" ]
        locker' = intercalate " " (l:lOpts)

-- | Spawns rofi to run in combi mode with drun and run.
spawnRofi :: MonadIO m => String -> Int -> m ()
spawnRofi f fSize= safeSpawn "rofi" [
    "-modi", "combi"
  , "-combi-modi", "drun,run"
  , "-show", "combi"
  , "-font", f ++ " " ++ show fSize ]

-- | Start the network manager applet.
networkManager :: MonadIO m => m ()
networkManager = safeSpawn "nm-applet" mempty

-- Screen capture

-- | Capture a selected section of the screen.
captureSelection :: MonadIO m => m ()
captureSelection = do
  t <- now
  spawn . concat $ ["maim -s $HOME/Pictures/screenshots/", t, ".png"]

-- | Capture the whole screen.
printScreen :: MonadIO m => m ()
printScreen = safeSpawn "scrot" ["$HOME/Pictures/screenshots/" ++ f ++ ".png"]
  where f = "%Y-%m-%d-%H:%M:%S"

-- Multimedia controls

-- | Toggles microphone on/off.
toggleMic :: MonadIO m => m ()
toggleMic = safeSpawn "amixer" ["set", "Capture", "toggle"]

-- | Toggles mute on/off.
toggleMute :: MonadIO m => m ()
toggleMute = safeSpawn "amixer" ["set", "Master", "toggle"]

-- | Raises the volume by a certain percent.
raiseVolumeBy :: MonadIO m => Int -> m ()
raiseVolumeBy p = safeSpawn "amixer" ["set", "Master", by]
  where by = concat [show p, "%+"]

-- | Lowers the volume by a certain percent.
lowerVolumeBy :: MonadIO m => Int -> m ()
lowerVolumeBy p = safeSpawn "amixer" ["set", "Master", by]
  where by = concat [show p, "%-"]

-- | Raises the brightness by a certain value.
raiseBrightnessBy :: MonadIO m => Int -> m ()
raiseBrightnessBy x = safeSpawn "xbacklight" ["-inc", show x]

-- | Lowers the brightness by a certain value.
lowerBrightnessBy :: MonadIO m => Int -> m ()
lowerBrightnessBy x = safeSpawn "xbacklight" ["-dec", show x]

-- | Determines if a process is running by name.
running :: MonadIO m => String -> m Bool
running s = null <$> runProcessWithInput "pgrep" [s] ""
