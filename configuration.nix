# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Allow unfree packages (for firmware).
  nixpkgs.config.allowUnfree = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.
  networking.networkmanager.enable = true;  # Enables networkmanager.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;

  # Uncomment if NetworkManager is not enabled:
  # networking.interfaces.enp0s20u1.useDHCP = true;

  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # Install fonts.
  fonts.fonts = with pkgs; [
    source-code-pro
    source-sans-pro
    source-serif-pro
  ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [

   # For WM
   i3status maim scrot light grim slurp xdg-user-dirs

   # Utilities.
   wget networkmanager git acpi pavucontrol tree bluez

   # Social.
   weechat tdesktop

   # Languages.
   stack racket

   # Terminals.
   termite

   # Graphical tools
   evince libreoffice keepassxc gimp

   # Music.
   mopidy mopidy-iris mopidy-local-sqlite mopidy-spotify
   mpd ncmpcpp

   # Editors.
   emacs vim

   # Browsers.
   firefox lynx
  ];

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable bluetooth
  hardware.bluetooth.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio = {
    enable = true;
    # NixOS allows either a lightweight build (default) or
    # full build of PulseAudio to be installed. Only the full
    # build has Bluetooth support, so it must be selected here.
    package = pkgs.pulseaudioFull;
  };

  # Enable Sway
  programs.sway.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.mitchell = {
    isNormalUser = true;
    extraGroups = [ "wheel" "sudo" "sway" "networkmanager" ];
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

}
