(package-initialize)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))

;; graphical configuration
(if (display-graphic-p) ; if in graphical mode
    (progn ; configure graphical mode
      (tool-bar-mode -1)  ; no tool bar
      (scroll-bar-mode -1)  ; no scroll bar
      (menu-bar-mode 0)  ; no menu bar
      (add-to-list 'default-frame-alist '(height . 40))
      (add-to-list 'default-frame-alist '(width . 85))
      (add-to-list 'default-frame-alist '(font . "Fira Code-12"))
      (set-face-attribute 'default t :font "Fira Code-12")
      (setq ring-bell-function 'ignore)
      (set-default 'cursor-type 'bar)
      (setq ns-pop-up-frames nil)  ; new files open in the same window
      (exec-path-from-shell-initialize)))  ; load $PATH

;; spacemacs prettification
(setq spacemacs-looks t)
(if spacemacs-looks
    (progn
      (require 'spaceline-config)
      (load-theme 'spacemacs-dark t)
      (spaceline-emacs-theme)))

;; use-package declarations
(eval-when-compile (require 'use-package))
(use-package htmlize :ensure t)
(use-package exec-path-from-shell :ensure t)
(use-package haskell-mode :ensure t)
(use-package pdf-tools :ensure t)
(use-package tex-mode :ensure auctex)
(use-package spaceline :ensure t)
(use-package spacemacs-common :ensure spacemacs-theme)

;; pdf-tools configuration
(pdf-tools-install)
(defvar prefer-pdf-tools (fboundp 'pdf-view-mode))
(defun start-pdf-tools-if-pdf ()
  (when (eq doc-view-doc-type 'pdf)
    (pdf-view-mode)))
(add-hook 'doc-view-mode-hook 'start-pdf-tools-if-pdf)

;; tex configuration
;; Use pdf-tools to open PDF files
(setq TeX-view-program-selection '((output-pdf "PDF Tools"))
      TeX-source-correlate-start-server t)
(setq TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view")))
(add-hook 'TeX-after-TeX-LaTeX-command-finished-hook
          #'TeX-revert-document-buffer)  ; update pdf bufs on command finish
(add-hook 'TeX-after-compilation-finished-functions
          #'TeX-revert-document-buffer)  ; update pdf bufs on compilation

;; haskell configuration
(add-hook 'haskell-mode-hook 'interactive-haskell-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
(define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-or-reload)
(define-key haskell-mode-map (kbd "C-`") 'haskell-interactive-bring)
(define-key haskell-mode-map (kbd "C-c C-t") 'haskell-process-do-type)
(define-key haskell-mode-map (kbd "C-c C-i") 'haskell-process-do-info)
(define-key haskell-mode-map (kbd "C-c C-c") 'haskell-process-cabal-build)
(define-key haskell-mode-map (kbd "C-c C-k") 'haskell-interactive-mode-clear)
(define-key haskell-mode-map (kbd "C-c c") 'haskell-process-cabal)
(setq haskell-process-type (quote stack-ghci))
(setq haskell-stylish-on-save t)  ; format Haskell on save

;; c configuration
(setq c-default-style "linux")

;; org configuration
(require 'org)
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-agenda-files (list "/home/mitchell/Dropbox/docs/org/planner.org"))
(setq org-src-fontify-natively t)
(setq org-log-done t)
(setq org-startup-with-inline-images t)
(setq org-export-backends (quote (ascii html icalendar latex md odt)))
(setq org-file-apps
      (quote
       ((auto-mode . emacs)
        ("\\.x?html?\\'" . "/usr/bin/firefox %s")
        ("\\.pdf\\'" . default))))
(add-hook 'org-mode-hook 'turn-on-auto-fill) ; word wrapping
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (http . t)))
(with-eval-after-load "ox-latex"
  (add-to-list 'org-latex-classes
               '("koma-article" "\\documentclass{scrartcl}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

;; yasnippet configuration
(require 'yasnippet)
(add-hook 'org-mode-hook 'yas-minor-mode)
(add-hook 'org-mode-hook '(lambda () (yas-reload-all)))

;; general editor configuration
(setq backup-directory-alist `(("." . "~/.saves")))
(setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))
(setq-default indent-tabs-mode nil)  ; spaces instead of tabs
(setq tab-stop-list (number-sequence 4 1000 4))  ; tabs = 4 spaces
(define-key text-mode-map (kbd "TAB") 'tab-to-tab-stop)  ; tab = next tabstop
(setq require-final-newline t)  ; instert newline at end of files
(auto-fill-mode -1)
(remove-hook 'text-mode-hook #'turn-on-autofill)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(setq whitespace-style '(face tabs lines-tail trailing))
(setq whitespace-line-column 79)
(global-whitespace-mode t)
(setq column-number-mode t)  ; display cursor column position
(setq default-directory "/home/mitchell/")
(setq inhibit-startup-message t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088"
     "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476"
     "8e797edd9fa9afec181efbfeeebf96aeafbd11b69c4c85fa229bb5b9f7f7e66c"
     "585942bb24cab2d4b2f74977ac3ba6ddbd888e3776b9d2f993c5704aa8bb4739"
     default)))
 '(hl-todo-keyword-faces
   (quote
    (("TODO" . "#dc752f")
     ("NEXT" . "#dc752f")
     ("THEM" . "#2d9574")
     ("PROG" . "#4f97d7")
     ("OKAY" . "#4f97d7")
     ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f")
     ("DONE" . "#86dc2f")
     ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d")
     ("HACK" . "#b1951d")
     ("TEMP" . "#b1951d")
     ("FIXME" . "#dc752f")
     ("XXX+" . "#dc752f")
     ("\\?\\?\\?+" . "#dc752f"))))
 '(package-selected-packages
   (quote
    (spaceline spacemacs-theme auctex pdf-tools slime-repl
               wc-mode git-commit doom-themes quelpa dired-sidebar
               all-the-icons-dired use-package elpy flymake-python-pyflakes
               coffee-mode ob-http jedi smooth-scrolling yaml-mode leuven-theme
               exec-path-from-shell gruvbox-theme with-editor web-mode
               vimrc-mode toml-mode slime scala-mode2 rust-mode php-mode
               markdown-mode irfc haskell-mode go-mode apache-mode)))
 '(pdf-view-midnight-colors (quote ("#282828" . "#f2e5bc"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
